public class Main {
    public static void main(String[] args) {
        short height = 187;
        System.out.println("Man " + height + " is " + Fit.manWeight(height));
        System.out.println("Woman " + height + " is " + Fit.womanWeight(height));
    }
}
